all: patch_boostrap_source install_node_things install_ruby_things build_bootstrap

patch_boostrap_source:
	sed -i 's%scss/bootstrap.scss%../custom.scss%g' bootstrap/package.json

install_node_things:
	cd bootstrap && npm install

install_ruby_things:
	cd bootstrap && bundle install

build_bootstrap:
	cd bootstrap && npm run dist
